const mongoose = require('mongoose')
const UserSchema = new mongoose.Schema({
    userID : {
        type : String,
    },
    fullName : {
        type : String,
        maxLength : 100,
        minLength : 3,
        required : true
    },
    email : {
        type : String,
        required :true,
        unique : true,
        trim : true,
    },
    password : {
        type : String,
        required : true,
    },
    token : {
        type : String,
    },
    mobile : {
        type : Number,
        minLength : 10
    },
    role : {
        type : Number,
        default : 2,
    },
    address : {
        type : String,
        maxLength : 250
    },
    pan : {
        type : String,
        maxLength : 10
    } 
})

const User = mongoose.model("User",UserSchema)
module.exports = User
