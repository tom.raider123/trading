const express = require('express')
const bodyParser = require('body-parser')
const app = express()
const port = 8000
const DBconnect = require('./db/connect')
const Axios  = require ('axios')
require('dotenv').config()
app.use(express.json())
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({extended :false}) ) 

const { notFound , errorHandler } = require('./middlewares/errorHandler')

const user = require('./routes/userRoutes')
const login = require('./routes/loginRoutes')
const market = require('./routes/marketRoutes')
const verifyToken = require('./middlewares/verifyToken')

// app.use(notFound)
app.use(errorHandler)

// routes 
app.use('/api/v1/user',login)
app.use('/api/v1/user',verifyToken,user)
app.use('/api/v1/login',verifyToken,market)


app.get('/',(req,res)=>{
})

const start = async () => {
try{
    await DBconnect(process.env.MONGO_URI) 
    app.listen(port , console.log(`server runnung on port: ${port}`))
}catch(error){
    console.log(error); 
} 
}
start()  