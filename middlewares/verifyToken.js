const jwt = require('jsonwebtoken')


const auth = (req, res, next) => {
    try {
        const token = req.header("Authorization")
        if(!token) return res.status(400).json({status: false,msg: "Invalid Authentication."})
        jwt.verify(token, process.env.ACCESS_TOKEN_SECRET, (err, user) => {
            if(err) return res.status(400).json({status: true,msg: "Invalid Authentication."})

            req.user = user
            next()
        })
    } catch (err) {
        return res.status(500).json({msg: err.message})
    }
}




// const authMiddleware = asyncHandler(async(req,res)=>{
      
//     let token;

//     if(req?.headers?.authorization?.startsWith('Bearer')){
// token = req.header.authorization.split(" ")[1]
// try{
//     if(token){
//         const decod = jwt.verify(token,"trading")
//     console.log("decode")
//     }

// }
// catch(error){
     
// throw new Error("token expired please login again")
// }
//     }else{
//         throw new Error("There is no token attached")

//     }
//  })

module.exports = auth
