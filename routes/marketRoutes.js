const express = require('express')
const { get } = require('mongoose')
const router = express.Router()

const getMarketData = require('./../controllers/marketController')
router.route('/:query/:symbol:/intervel').get(getMarketData)

module.exports = router 