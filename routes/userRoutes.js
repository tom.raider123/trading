const express = require('express')
const router  = express.Router()
const {
    getAllUser,
    getUser,
    updateUser,
    deleteUser,
    forgetPassword,
    logOut,
    updateProfile
} = require('../controllers/userController')



router.get('/get-all-user',getAllUser)
router.get('/:id',getUser)
router.patch('/:id',updateUser)
router.delete('/:id',deleteUser)
router.post('/forget-password',forgetPassword)
router.get('/logout',logOut)
router.put('/:id',updateProfile)

module.exports = router